const http = require("http");
const express = require("express");
const bodyParser = require("body-parser");

const users = require("./routes/users");
const orders = require("./routes/orders");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

app.use(users);
app.use(orders);

app.use((req, res, send) => {
  res.status(404).send("Page not found.");
});

app.listen(3000);
