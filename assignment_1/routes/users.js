const express = require("express");

const router = express.Router();

router.get("/", (req, res) => {
  console.log("Welcome to the initial project");
});

router.use('/login',(req,res)=>{
    console.log(`Login email: ${req.body.email}`);
    console.log(`Login password: ${req.body.password}`);
    res.send('User credentials received');
})

module.exports = router;
