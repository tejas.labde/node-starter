const http=require('http');
const express=require('express');

const app=express();

app.use((req,res,send)=>{
    //allows request to go to the next middleware present in the file
    // next();
    //allows us to send data in the response to a particular request.
    // res.send({});
    })

const server=http.createServer(app);
server.listen(3000);